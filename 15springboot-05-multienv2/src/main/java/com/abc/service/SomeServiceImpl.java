package com.abc.service;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月20日 </p>
 * <p>类全名：com.abc.service.SomeServiceImpl</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Service
@Profile(value = "pro")
public class SomeServiceImpl implements SomeService {
    @Override
    public String hello() {
        return "hello SpringBoot pro world!";
    }
}
