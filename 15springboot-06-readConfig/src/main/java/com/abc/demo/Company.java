package com.abc.demo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月22日 </p>
 * <p>类全名：com.abc.Company</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @verstion 1.0
 */
@Data
@Component
@PropertySource(value = "classpath:other.properties", encoding = "UTF-8")
@ConfigurationProperties("lrz.company")
public class Company {
    private Integer id;
    private String name;
    @Override
    public String toString() {
        return String.format("id=%s,name=%s",id,name);
    }
}
