package com.abc.controller;

import com.abc.demo.Company;
import com.abc.demo.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月15日 </p>
 * <p>类全名：com.abc.controller.SomeController</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@RestController
public class SomeController {

    @Value("${server.port}")
    private String serverport;

    @GetMapping(value = "/sayhello")
    public String hello() {
        return "hello SpringBoot world!" + serverport;
    }

    @GetMapping(value = "/serverport")
    public String serverport() {
        return serverport;
    }

    @Autowired
    private Company company;
    @GetMapping(value = "/getCompany")
    public String getCompan() {
        return company.toString();
    }
    @Autowired
    private Country country;
    @GetMapping(value = "/getCountry")
    public String getCountry() {
        return "country="+country;
    }
}
