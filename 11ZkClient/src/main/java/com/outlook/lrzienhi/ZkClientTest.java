package com.outlook.lrzienhi;

import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.serialize.SerializableSerializer;
import org.apache.zookeeper.CreateMode;

import java.util.List;

public class ZkClientTest {
    //指定ZK集群
    private static final String CLUSTER = "192.168.0.107:2181";
    //指定节点名称
    private static final String PATH="/ZkClient";

    public static void main(String[] args) {
        //--------------1、创建会话--------------

        //创建zkClient
        ZkClient zkClient = new ZkClient(CLUSTER);
        //为zkClinet指定序列化器
        zkClient.setZkSerializer(new SerializableSerializer());

        //--------------2、创建节点--------------
        //创建持久节点 /ZkClient ,值为 zkClient first
        String nodeName = zkClient.create(PATH, "zkClient first", CreateMode.PERSISTENT);
        System.out.println(String.format("新创建的节点名称为：%s", nodeName));
        //--------------3、获取节点内容--------------
        Object readData = zkClient.readData(PATH);
        System.out.println(String.format("节点的数据内容为：%s", readData));
        //--------------4、注册watcher--------------
        zkClient.subscribeDataChanges(PATH, new IZkDataListener() {
            @Override
            public void handleDataChange(String s, Object o) throws Exception {
                System.out.println(String.format("节点%s数据内容更新为：%s", s, o.toString()));
            }

            @Override
            public void handleDataDeleted(String s) throws Exception {
                System.out.println(String.format("节点%s的数据内容已删除！", s));
            }
        });

        //--------------5、更新节点内容--------------
        zkClient.writeData(PATH, "ZkClient Twice");
        Object updateData = zkClient.readData(PATH);
        System.out.println(String.format("节点%s更新后的数据内容为%s", nodeName, updateData));
        //--------------6、删除节点--------------
        zkClient.delete(PATH);

        //--------------7、判断节点是否还存在--------------
        boolean isExists = zkClient.exists(PATH);
        System.out.println(String.format("节点%s仍存在吗？%s", nodeName, isExists?"存在":"不存在"));
    }
}
