package com.abc.controller;

import com.abc.bean.Student;
import com.abc.service.ISomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月15日 </p>
 * <p>类全名：com.abc.controller.SomeController</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Controller
@RequestMapping("/test")
public class SomeController {

    @Autowired
    private ISomeService iSomeService;

    @RequestMapping(value = "/register")
    public String resgister(Student student, Model model) throws Exception {
        model.addAttribute("student",student);
        iSomeService.addStudent(student);
        return "/WEB-INF/welcome";
    }

    @RequestMapping(value = "/index")
    public String index() {
        return "/WEB-INF/index";
    }
}
