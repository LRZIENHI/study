package com.abc.dao;

import com.abc.bean.Student;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年08月11日 </p>
 * <p>类全名：com.abc.controller.IStudentDao</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Mapper
public interface StudentDao {
    void insertStudent(Student student);
}
