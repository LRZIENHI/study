package com.abc.service;

import com.abc.bean.Student;

public interface ISomeService  {
    void addStudent(Student student) throws Exception;
}
