package com.abc.service;

import com.abc.bean.Student;
import com.abc.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年08月11日 </p>
 * <p>类全名：com.abc.controller.SomeServiceImpl</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Service
public class SomeServiceImpl implements ISomeService {
    @Autowired
    private StudentDao dao;

    // 采用Spring默认的事务提交方式：发生运行时异常回滚，发生受查异常提交
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addStudent(Student student) throws Exception {
        dao.insertStudent(student);
        int i=0;
        if(true) {
            throw new Exception("发生受查异常");
        }
        dao.insertStudent(student);
    }
}
