package com.outlook.lrz;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.CuratorWatcher;
import org.apache.curator.framework.api.GetDataBuilder;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;

import java.util.List;

public class CuratorTest {
    public static void main(String[] args) throws Exception {
        //--------------1、创建会话-------------
        //创建重试策略对象： 第1秒重试1次，最多重试3次
        ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(1000, 3);
        //创建客户端
        CuratorFramework client = CuratorFrameworkFactory.builder()
                .connectString("192.168.0.107:2181")
                .sessionTimeoutMs(15000)
                .connectionTimeoutMs(13000)
                .retryPolicy(retryPolicy)
                .namespace("host")
                .build();
        //开启客户端
        client.start();
        //--------------2、创建节点-------------
        //指定要创建和操作的节点，注意，其是相对/host 节点的
        String nodePath = "/host2";
        //创建一个持久节点并设置值为myhost
        String nodeName = client.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).forPath("/host2", "myhost".getBytes());
        System.out.println(String.format("新创建的节点名称为：%s", nodeName));
        //String nodeName2 = client.create().forPath(nodePath);

        //--------------3、获取数据内容并注册watcher-------------
        //Curator中绑定watch的操作有3种：getData()、getChildren()、checkExists()
        byte[] bytes = client.getData().usingWatcher((CuratorWatcher) x -> System.out.println(String.format("节点%s数据内容更新", x.getPath()))).forPath(nodePath);
        System.out.println(String.format("节点的数据内容为：%s", new String(bytes)));
        //--------------4、更新数据内容-------------
        client.setData().forPath(nodePath, "newhost".getBytes());
        //获取更新后的数据
        byte[] newData = client.getData().forPath(nodePath);
        System.out.println(String.format("节点%s更新后的数据内容为%s", nodeName, new String(newData)));
        List<String> childrens = client.getChildren().forPath(nodePath);
        //--------------5、删除节点-------------
        client.delete().forPath(nodePath);
        client.delete().deletingChildrenIfNeeded().forPath(nodePath);
        //--------------6、判断节点存在性-------------
        Stat stat = client.checkExists().forPath(nodePath);
        boolean isExists = stat!=null;
        System.out.println(String.format("节点%s仍存在吗？%s", nodeName, isExists?"存在":"不存在"));
    }
}
