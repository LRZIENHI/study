package com.abc.wrapertest.controller;

import com.lrz.service.WrapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月05日 </p>
 * <p>类全名：com.abc.wrapertest.controller.WrapController</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@RestController
public class WrapController {
    @Autowired
    private WrapService service;
    @RequestMapping("/wrap/{param}")
    public String wrapHandler(@PathVariable String param) {
        return  service.wrap(param);
    }
}
