package com.outlook.lrz.charp01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.junit.Test;

/**
 * <p>标题：原生JDBC测试 </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp01.JdbcTest</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class JdbcTest {

    @Test
    public void execut() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://39.102.42.229:3306/cloudfikj0319","root", "cp6mhsLPmGLyuS");
            Statement statement = connection.createStatement();
            boolean b = statement.execute("SELECT * from t_pm_user where name='admin'");
            System.out.println(b);
            statement.close();
            connection.close();
        } catch (Exception e) {

        } finally {

        }

    }
}
