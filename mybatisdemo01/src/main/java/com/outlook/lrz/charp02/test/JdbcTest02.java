package com.outlook.lrz.charp02.test;

import org.junit.Test;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

/**
 * <p>标题：properties配置数据库版本 </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp02.JdbcTest02</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class JdbcTest02 {

    public Properties loadProfile() throws Exception{
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("jdbc.properties");
        Properties properties = new Properties();
        properties.load(inputStream);
        return properties;
    }

    @Test
    public void execut() {
        try {
            Properties properties = loadProfile();
            Class.forName(properties.getProperty("jdbc.class"));
            Connection connection = DriverManager.getConnection(properties.getProperty("jdbc.url"),properties.getProperty("jdbc.name"),properties.getProperty("jdbc.password"));
            Statement statement = connection.createStatement();
            boolean b = statement.execute("SELECT * from t_pm_user where name='admin'");
            System.out.println(b);
            statement.close();
            connection.close();
        } catch (Exception e) {

        } finally {

        }

    }
}
