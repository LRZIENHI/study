package com.outlook.lrz.charp03.framework.sqlnode;

import com.outlook.lrz.charp03.framework.utils.GenericTokenParser;
import com.outlook.lrz.charp03.framework.utils.OgnlUtils;
import com.outlook.lrz.charp03.framework.utils.SimpleTypeRegistry;
import com.outlook.lrz.charp03.framework.utils.TokenHandler;

/**
 * <p>标题： </p>
 * <p>功能：封装的是带有${}的文本字符串 </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp03.framework.sqlnode.TextSqlNode</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class TextSqlNode implements SqlNode {
    private String sqlText;
    public TextSqlNode(String sqlText) {
        this.sqlText = sqlText;
    }

    // 文本中的${}，在此处的处理
    @Override
    public void apply(DynamicContext context) {
        BindingTokenParser handler = new BindingTokenParser(context);
        // 将#{}解析为?并保存参数信息
        GenericTokenParser tokenParser = new GenericTokenParser("${", "}", handler);
        // 获取真正可以执行的SQL语句
        String sql = tokenParser.parse(sqlText);
        context.appendSql(sql);
    }

    /**
     * 是否为动态SQL文本
     * @return
     */
    public boolean isDynamic() {
        return sqlText!=null && sqlText.contains("${");
    }

    private static class BindingTokenParser implements TokenHandler {
        private DynamicContext context;

        public BindingTokenParser(DynamicContext context) {
            this.context = context;
        }

        /**
         * expression：比如说${username}，那么expression就是username username也就是Ognl表达式
         */
        @Override
        public String handleToken(String expression) {
            Object paramObject = context.getBindings().get("_parameter");
            if (paramObject == null) {
                // context.getBindings().put("value", null);
                return "";
            } else if (SimpleTypeRegistry.isSimpleType(paramObject.getClass())) {
                // context.getBindings().put("value", paramObject);
                return String.valueOf(paramObject);
            }

            // 使用Ognl api去获取相应的值
            Object value = OgnlUtils.getValue(expression, context.getBindings());
            String srtValue = value == null ? "" : String.valueOf(value);
            return srtValue;
        }

    }
}

