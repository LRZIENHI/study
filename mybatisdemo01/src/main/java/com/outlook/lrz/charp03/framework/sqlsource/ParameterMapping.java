package com.outlook.lrz.charp03.framework.sqlsource;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp03.framework.sqlsource.ParameterMapping</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
@EqualsAndHashCode
@Accessors(chain = true)
public class ParameterMapping {
    private String name;
    private Class<?> type;

    public ParameterMapping(String name) {
        this.name = name;
    }
}
