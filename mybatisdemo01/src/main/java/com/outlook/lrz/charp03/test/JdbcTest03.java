package com.outlook.lrz.charp03.test;

import com.amazonaws.util.StringUtils;
import com.google.common.collect.Lists;
import com.outlook.lrz.charp03.framework.mapping.BoundSql;
import com.outlook.lrz.charp03.framework.mapping.MappedStatement;
import com.outlook.lrz.charp03.framework.sqlnode.*;
import com.outlook.lrz.charp03.framework.sqlsource.DynamicSqlSource;
import com.outlook.lrz.charp03.framework.sqlsource.ParameterMapping;
import com.outlook.lrz.charp03.framework.sqlsource.RawSqlSource;
import com.outlook.lrz.charp03.framework.sqlsource.SqlSource;
import com.outlook.lrz.charp03.framework.session.Configuration;
import com.outlook.lrz.charp03.framework.user.dao.User;
import com.outlook.lrz.charp03.framework.utils.DocumentUtils;
import com.outlook.lrz.charp03.framework.utils.SimpleTypeRegistry;
import org.apache.commons.dbcp.BasicDataSource;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.Text;
import org.junit.Test;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp03.test.JdbcTest03</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class JdbcTest03 {

    Configuration configuration = new Configuration();

    public void loadConfiguration() throws Exception{
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml");
        Document document = DocumentUtils.readDocument(inputStream);
        Element element = document.getRootElement();
        parseConfiguration(element);
    }

    private void parseConfiguration(Element rootElement) {
        Element environments = rootElement.element("environments");
        parseEnv(environments);

        Element mappers = rootElement.element("mappers");
        parseMappers(mappers);
    }

    private void parseEnv(Element environments) {
        String defaultV = environments.attributeValue("default");
        List<Element> envList = environments.elements("environment");
        for (Element envElement : envList) {
            String id = envElement.attributeValue("id");
            if(defaultV.equals(id)) {
                parseEnvironment(envElement);
            }
        }
    }

    private void parseEnvironment(Element envElement) {
        Element dataSourceElement = envElement.element("dataSource");
        parseDataSourceElement(dataSourceElement);
    }

    private void parseDataSourceElement(Element dataSourceElement) {
        String type = dataSourceElement.attributeValue("type");
        if("DBCP".equals(type)) {
            List<Element> elements = dataSourceElement.elements();
            Properties properties = new Properties();
            for (Element property : elements) {
                String name = property.attributeValue("name");
                String value = property.attributeValue("value");
                properties.put(name,value);
            }
            BasicDataSource dataSource = new BasicDataSource();
            dataSource.setDriverClassName(properties.get("class").toString());
            dataSource.setUrl(properties.get("url").toString());
            dataSource.setUsername(properties.get("name").toString());
            dataSource.setPassword(properties.get("password").toString());
            configuration.setDataSource(dataSource);
        }
    }

    private void parseMappers(Element mappers) {
        List<Element> mapperList = mappers.elements();
        for (Element mapper : mapperList) {
            String resource = mapper.attributeValue("resource");
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(resource);
            Document mapperXml = DocumentUtils.readDocument(inputStream);
            parseXmlMapper(mapperXml.getRootElement());
        }

    }

    private void parseXmlMapper(Element rootElement) {
        String namespace = rootElement.attributeValue("namespace");
        List<Element> selectElements = rootElement.elements("select");
        for (Element selectElement : selectElements) {
            parseStatementElement(namespace,selectElement);
        }
    }

    private void parseStatementElement(String namespace, Element selectElement) {
        String statementId = namespace + "." + selectElement.attributeValue("id");
        if (statementId == null || selectElement.equals("")) {
            return;
        }
        MappedStatement mappedStatement = new MappedStatement();
        mappedStatement.setStatementid(statementId);
        String parameterType = selectElement.attributeValue("parameterType");
        if(!StringUtils.isNullOrEmpty(parameterType)) {
            Class<?> paramterClass = getaClass(parameterType);
            mappedStatement.setParamterType(paramterClass);
        }
        String resultType = selectElement.attributeValue("resultType");
        if(!StringUtils.isNullOrEmpty(resultType)) {
            Class<?> resultClass = getaClass(resultType);
            mappedStatement.setResultType(resultClass);
        }
        String statementType = selectElement.attributeValue("statementType");
        mappedStatement.setStatementType(StringUtils.isNullOrEmpty(statementType)?"prepared":statementType);
        // 解析SQL信息
        SqlSource sqlSource = createSqlSource(selectElement);
        mappedStatement.setSqlSource(sqlSource);
        configuration.putMappedStatement(mappedStatement);
    }

    private SqlSource createSqlSource(Element selectElement) {
        SqlSource sqlSource = parseScriptNode(selectElement);
        return sqlSource;
    }

    private SqlSource parseScriptNode(Element selectElement) {
        SqlNode mixedSqlNode = parseDynamicTags(selectElement);
        // isDynamic是parseDynamicTags过程中，得到的值
        // 如果包含${}或者包含动态标签，则isDynamic为true
        SqlSource sqlSource;
        if (isDynamic) {
            sqlSource = new DynamicSqlSource(mixedSqlNode);
        } else {
            sqlSource = new RawSqlSource(mixedSqlNode);
        }
        return sqlSource;
    }
    boolean isDynamic = false;
    private MixedSqlNode parseDynamicTags(Element selectElement) {
        List<SqlNode> sqlNodes = Lists.newArrayList();
        int count = selectElement.nodeCount();
        for(int i=0;i<count;i++) {
            Node node = selectElement.node(i);
            if(node instanceof Text) {//文本标签
                String text = ((Text)node).getText();
                TextSqlNode textSqlNode = new TextSqlNode(text);
                if(textSqlNode.isDynamic()) {
                    isDynamic = true;
                    sqlNodes.add(textSqlNode);
                } else {
                    StaticTextSqlNode staticTextSqlNode = new StaticTextSqlNode(text);
                    sqlNodes.add(staticTextSqlNode);
                }
            } else if(node instanceof Element) {//动态标签
                // 获取动态标签的标签名称
                String nodeName = node.getName();
                Element nodeElement = (Element) node;
                if("if".equals(nodeName)) {
                    String test = nodeElement.attributeValue("test");
                    MixedSqlNode mixedSqlNode = parseDynamicTags((Element) node);
                    IfSqlNode ifSqlNode = new IfSqlNode(test, mixedSqlNode);
                    sqlNodes.add(ifSqlNode);
                } else if("where".equals(nodeName)) {

                } else if("".equals(nodeName)) {

                }
                isDynamic = true;
            }
        }
        return new MixedSqlNode(sqlNodes);
    }

    private Class<?> getaClass(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }



    public List<Object> execut(String statementId, Object parameter) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            loadConfiguration();

            //1、获取connection
            connection = configuration.getDataSource().getConnection();
            //2、获取SQL
            MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
            if(mappedStatement==null) {//没有找到需要执行的sql
                return null;
            }
            SqlSource sqlSource = mappedStatement.getSqlSource();
            BoundSql boundSql = sqlSource.getBoundSql(parameter);
            //获取带有占位符？的SQL
            String sql = boundSql.getSql();
            //3、获取Statement
            String statementType = mappedStatement.getStatementType();
            if("prepared".equals(statementType)) {
                // 3、获取statement
                statement = connection.prepareStatement(sql);
                // 4、设置参数
                handleParameter(statement, mappedStatement, boundSql, parameter);

                // 5、向数据库发出 sql 执行查询，查询出结果集
                rs = statement.executeQuery();

                // 6、处理结果集
                return handleResultSet(rs, mappedStatement);
            } else if("...".equals(statementType)) {

            }

            return null;
        } catch (Exception e) {

        } finally {
            if(rs!=null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(statement!=null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(connection!=null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private List<Object> handleResultSet(ResultSet rs, MappedStatement mappedStatement) throws Exception {
        List<Object> results = Lists.newArrayList();
        Class<?> resultTypeClass = mappedStatement.getResultType();
        while (rs.next()) {
            // 遍历一次是一行，也对应一个对象，利用反射new一个对象
            Object result = resultTypeClass.newInstance();

            // 要获取每一列的值，然后封装到结果对象中对应的属性名称上
            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();
            for (int i = 0; i < columnCount; i++) {
                // 获取每一列的值
                Object value = rs.getObject(i + 1);

                // 列的名称
                String columnName = metaData.getColumnName(i + 1);
                // 列名和属性名称要严格一致
                Field field = resultTypeClass.getDeclaredField(columnName);
                field.setAccessible(true);
                // 给映射的对象赋值
                field.set(result, value);
            }
            results.add(result);
        }

        return results;
    }

    private void handleParameter(PreparedStatement preparedStatement, MappedStatement mappedStatement, BoundSql boundSql, Object parameter) throws Exception {
        // 从mappedStatement获取入参的类型
        Class<?> parameterTypeClass = mappedStatement.getParamterType();
        // 如果入参是8种基本类型和String类型
        if (SimpleTypeRegistry.isSimpleType(parameterTypeClass)) {
            preparedStatement.setObject(1, parameter);
        } else if (parameterTypeClass == Map.class) {
            // 如果入参是Map类型
            // ....
        } else {
            // 如果入参是POJO类型（比如User类型）
            List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
            for (int i = 0; i < parameterMappings.size(); i++) {
                ParameterMapping parameterMapping = parameterMappings.get(i);
                // 封装的#{}里面的属性名称
                String name = parameterMapping.getName();
                // 利用反射去入参对象根据属性名称获取指定的属性值
                Field field = parameterTypeClass.getDeclaredField(name);
                field.setAccessible(true);
                Object value = field.get(parameter);
                // TODO 可以使用ParameterMapping里面的type对Object类型的value进行类型处理
                // 设置statement占位符中的值
                preparedStatement.setObject(i + 1, value);
            }
        }
    }

    @Test
    public void test() {
        List<Object> lists = execut("User.findById", new User().setNumber("admin"));
        if(lists!=null && lists.size()>0) {
            for (Object list : lists) {
                System.out.println(list);
            }
        }
    }
}
