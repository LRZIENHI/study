package com.outlook.lrz.charp03.framework.mapping;

import com.outlook.lrz.charp03.framework.sqlsource.SqlSource;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp03.framework.mapping.MappedStatement</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
@EqualsAndHashCode
@Accessors(chain = true)
public class MappedStatement {
    private String statementid;
    private Class<?> paramterType;
    private Class<?> resultType;
    private String statementType;
    private SqlSource sqlSource;
}
