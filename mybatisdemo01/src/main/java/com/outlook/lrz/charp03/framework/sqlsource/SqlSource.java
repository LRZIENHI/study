package com.outlook.lrz.charp03.framework.sqlsource;

import com.outlook.lrz.charp03.framework.mapping.BoundSql;

public interface SqlSource {
    BoundSql getBoundSql(Object parameterObject);
}
