package com.outlook.lrz.charp03.framework.sqlsource;

import com.outlook.lrz.charp03.framework.mapping.BoundSql;
import com.outlook.lrz.charp03.framework.sqlnode.DynamicContext;
import com.outlook.lrz.charp03.framework.sqlnode.SqlNode;
import com.outlook.lrz.charp03.framework.utils.GenericTokenParser;
import com.outlook.lrz.charp03.framework.utils.ParameterMappingTokenHandler;

/**
 * <p>标题： </p>
 * <p>功能：封装带有${}或者动态SQL标签的SQL信息 </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp03.framework.sqlsource.DynamicSqlSource</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class DynamicSqlSource implements SqlSource {
    private SqlNode rootSqlNode;
    public DynamicSqlSource(SqlNode mixedSqlNode) {
        this.rootSqlNode = mixedSqlNode;
    }

    @Override
    public BoundSql getBoundSql(Object parameterObject) {
        //由于DynamicSqlSource 封装的是带有${}的动态sql，所以需要传入参数进行sql拼接
        DynamicContext context = new DynamicContext(parameterObject);
        // 处理SqlNode，先去处理动态标签和${}，拼接成一条SQL文本，该SQL文本还包含#{}
        rootSqlNode.apply(context);
        // 处理#{}
        ParameterMappingTokenHandler handler = new ParameterMappingTokenHandler();
        // 将#{}解析为?并保存参数信息
        GenericTokenParser tokenParser = new GenericTokenParser("#{", "}", handler);
        // 获取真正可以执行的SQL语句
        String sql = tokenParser.parse(context.getSql());

        return new BoundSql(sql, handler.getParameterMappings());
    }
}
