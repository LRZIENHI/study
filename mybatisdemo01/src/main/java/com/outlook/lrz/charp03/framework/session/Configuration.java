package com.outlook.lrz.charp03.framework.session;

import com.google.common.collect.Maps;
import com.outlook.lrz.charp03.framework.mapping.MappedStatement;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.sql.DataSource;
import java.util.Map;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp03.framework.session.Configuration</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
@EqualsAndHashCode
@Accessors(chain = true)
public class Configuration {
    private DataSource dataSource;
    private Map<String,MappedStatement> mappedStatementMap = Maps.newHashMap();

    public void putMappedStatement(MappedStatement mappedStatement) {
        mappedStatementMap.put(mappedStatement.getStatementid(), mappedStatement);
    }
}
