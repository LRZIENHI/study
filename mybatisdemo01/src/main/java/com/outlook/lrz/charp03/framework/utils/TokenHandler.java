package com.outlook.lrz.charp03.framework.utils;

public interface TokenHandler {
    String handleToken(String content);
}
