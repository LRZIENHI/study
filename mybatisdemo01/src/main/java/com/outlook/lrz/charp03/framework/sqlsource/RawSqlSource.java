package com.outlook.lrz.charp03.framework.sqlsource;

import com.outlook.lrz.charp03.framework.mapping.BoundSql;
import com.outlook.lrz.charp03.framework.sqlnode.DynamicContext;
import com.outlook.lrz.charp03.framework.sqlnode.SqlNode;
import com.outlook.lrz.charp03.framework.utils.GenericTokenParser;
import com.outlook.lrz.charp03.framework.utils.ParameterMappingTokenHandler;

/**
 * <p>标题： </p>
 * <p>功能：
 * 封装最多只带有#{}的SQL信息
 * 也就是#{}需要被处理一次就可以，就可以使用占位符来长期使用。
 * 而这一点和${}很不一样。${}每一次被调用时，就需要去解析一次SQL语句
 * </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp03.framework.sqlsource.RawSqlSource</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class RawSqlSource implements  SqlSource {
    private StaticSqlSource staticSqlSource;

    public RawSqlSource(SqlNode mixedSqlNode) {

        // #{}处理的时候，不需要入参对象的支持
        DynamicContext context = new DynamicContext(null);
        // 处理SqlNode
        mixedSqlNode.apply(context);

        ParameterMappingTokenHandler handler = new ParameterMappingTokenHandler();
        // 将#{}解析为?并保存参数信息
        GenericTokenParser tokenParser = new GenericTokenParser("#{", "}", handler);
        // 获取真正可以执行的SQL语句
        String sql = tokenParser.parse(context.getSql());

        // 该SqlSource就是封装已经解析完成的Sql语句
        staticSqlSource = new StaticSqlSource(sql, handler.getParameterMappings());
    }

    @Override
    public BoundSql getBoundSql(Object parameterObject) {
        //由于#{}只需要被处理一次，所以直接放到了构造法法中构建staticSqlSource
        return staticSqlSource.getBoundSql(parameterObject);
    }
}
