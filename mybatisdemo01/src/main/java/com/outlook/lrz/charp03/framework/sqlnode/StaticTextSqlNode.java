package com.outlook.lrz.charp03.framework.sqlnode;

/**
 * <p>标题： </p>
 * <p>功能： 封装的是带有#{}的文本字符串 </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp03.framework.sqlnode.StaticTextSqlNode</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class StaticTextSqlNode implements SqlNode {
    private String sqlText;
    public StaticTextSqlNode(String text) {
        this.sqlText = text;
    }

    @Override
    public void apply(DynamicContext context) {
        context.appendSql(sqlText);
    }
}
