package com.outlook.lrz.charp03.framework.sqlnode;

import java.util.HashMap;

/**
 * <p>标题： </p>
 * <p>功能：动态上下文，就是封装的入参信息，解析过程中的SQL信息 </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年03月21日 </p>
 * <p>类全名：com.outlook.lrz.charp03.framework.sqlnode.DynamicContext</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class DynamicContext {
    //用于拼接sql
    private StringBuffer sb = new StringBuffer();
    //参数
    private HashMap<String, Object> bindings = new HashMap<>();

    public DynamicContext(Object param) {
        bindings.put("_parameter", param);
    }

    public String getSql() {
        return sb.toString();
    }

    public void appendSql(String sqlText) {
        this.sb.append(sqlText);
        this.sb.append(" ");
    }

    public HashMap<String, Object> getBindings() {
        return bindings;
    }

    public void addBinding(String name, Object param) {
        this.bindings.put(name, param);
    }
}
