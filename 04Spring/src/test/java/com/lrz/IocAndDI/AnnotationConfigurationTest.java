package com.lrz.IocAndDI;

import com.lrz.annotationConfiguration.Controller.UserController;
import com.lrz.annotationConfiguration.ImportConfiguration;
import com.lrz.annotationConfiguration.SpringConfiguration;
import com.lrz.annotationConfiguration.model.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月19日 </p>
 * <p>类全名：com.lrz.IocAndDI.AnnotationConfigurationTest</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class AnnotationConfigurationTest {
    @Test
    public void printlnUser() {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        //@Configuration配置的容器中，定义Bean对象
        System.out.println(context.getBean(User.class).toString());
        //扫描获取对象，对象中引入properties文件
        System.out.println(context.getBean(UserController.class).printUser());

//        ApplicationContext context2 = new AnnotationConfigApplicationContext(ImportConfiguration.class);
//        System.out.println(context2.getBean(User.class).toString());
    }
}
