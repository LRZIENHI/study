package com.lrz.IocAndDI;

import com.lrz.mixedConfiguration.controller.CityController;
import com.lrz.mixedConfiguration.model.City;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * <p>标题：混合配置（注解+xml） </p>
 * <p>功能：IoC 和 DI 通过混合配置方式实现
 *  1、IoC实现：在xml配文件中通过设置
 *          <context:component-scan base-package="com.lrz.mixedConfiguration.**"/>
 *          开启注解，并扫描指定包中文件的注解，获取实例
 *  2、DI依赖注入实现：
 *          通过@Autowired、@Resource、@Inject 实现对象的注入
 *              @Autowired与@Inject都是通过类型注入
 *              @Resource是通过名称注入
 *          通过@Value("${xxx}") 实现配置文件定义key的注入,xml文件中通过
 *           <context:property-placeholder location="classpath:city.properties" file-encoding="UTF-8"/>
 *           指定引入文件，并设置编码
 * </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月17日 </p>
 * <p>类全名：com.lrz.IocAndDI.MixedConfigurationTest</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:mixedConfiguration.xml"})
public class MixedConfigurationTest {
    @Autowired
    private CityController cityController;
    @Resource(name = "cityController1")
    private CityController cityController2;

    public static void main(String[] args) {

    }
    @Test
    public void city() {
        System.out.println("AResource标签根据名称匹配对象====="+cityController2.defaultCity());
        System.out.println("Autowired标签根据类型匹配对象====="+cityController.city());
        System.out.println("AResource标签根据名称匹配对象====="+cityController2.defaultCity());
    }
}
