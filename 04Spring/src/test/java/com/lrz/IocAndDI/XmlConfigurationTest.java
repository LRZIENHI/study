package com.lrz.IocAndDI;

import com.lrz.xmlConfiguration.model.Company;
import com.lrz.xmlConfiguration.model.Persion;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * <p>标题：Xml配置测试 </p>
 * <p>功能：IoC 和 DI 通过xml配置方式实现 </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月16日 </p>
 * <p>类全名：com.lrz.Test1</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class XmlConfigurationTest {


    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("xmlConfiguration.xml");
        Ioc(ctx);
        DI(ctx);
    }

    /**
     * xml配置控制反转的实现
     */
    private static void Ioc(ApplicationContext ctx) {
        Persion persion = (Persion) ctx.getBean("persion");
        System.out.println(persion.toString());

        Persion persion3 = (Persion) ctx.getBean("persionFactory");
        System.out.println(persion3.toString());

        Persion persion4 = (Persion) ctx.getBean("persion4");
        System.out.println(persion4.toString());
    }

    /**
     * xml方式依赖注入的实现
     * @param ctx
     */
    private static void DI(ApplicationContext ctx) {
        Company company1 = (Company) ctx.getBean("company1");
        System.out.println(company1.toString());

        Company company2 = (Company) ctx.getBean("company2");
        System.out.println(company2.toString());

        Company company3 = (Company) ctx.getBean("company3");
        System.out.println(company3.toString());
    }
}
