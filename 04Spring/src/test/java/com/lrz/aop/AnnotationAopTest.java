package com.lrz.aop;

import com.lrz.annotationConfiguration.SpringConfiguration;
import com.lrz.annotationaop.AnnotationAop;
import com.lrz.annotationaop.AopConfiguration;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月20日 </p>
 * <p>类全名：com.lrz.aop.XmlAopTest</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class AnnotationAopTest {
    @Test
    public void print() {
        ApplicationContext context = new AnnotationConfigApplicationContext(AopConfiguration.class);
        context.getBean(AnnotationAop.class).toString();
    }
}
