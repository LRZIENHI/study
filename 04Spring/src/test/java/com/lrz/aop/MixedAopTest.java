package com.lrz.aop;

import com.lrz.mixedaop.MixedAop;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月20日 </p>
 * <p>类全名：com.lrz.aop.XmlAopTest</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:mixedaop.xml"})
public class MixedAopTest {
    @Autowired private MixedAop mixedAop;

    @Test
    public void print() {
        System.out.println(mixedAop.toString());
    }
}
