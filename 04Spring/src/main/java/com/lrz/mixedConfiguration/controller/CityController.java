package com.lrz.mixedConfiguration.controller;

import com.lrz.mixedConfiguration.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月17日 </p>
 * <p>类全名：com.lrz.mixedConfiguration.controller.CityController</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Controller("cityController1")
public class CityController {
    @Autowired
    private City city;

    public String city() {
        return city.setId("beijing").setName("北京").toString();
    }

    public String defaultCity() {
        return city.toString();
    }
}
