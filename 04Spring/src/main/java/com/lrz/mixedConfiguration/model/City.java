package com.lrz.mixedConfiguration.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月17日 </p>
 * <p>类全名：com.lrz.mixedConfiguration.City</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
@Accessors(chain = true)
@Component
public class City {
    @Value("${city.id}")
    private String id;
    @Value("${city.name}")
    private String name;
    @Override
    public String toString() {
        return String.format("id=%s,name=%s",id,name);
    }
}
