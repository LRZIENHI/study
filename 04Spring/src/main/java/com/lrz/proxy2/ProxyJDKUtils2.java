package com.lrz.proxy2;

import com.lrz.proxy.model.Animal;
import com.lrz.proxy.service.AnimalService;
import com.lrz.proxy.service.AnimalServiceImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月12日 </p>
 * <p>类全名：com.lrz.proxy2.ProxyJDKUtils</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class ProxyJDKUtils2 {
    public static <T>T getProxy(T t) {
        return (T) Proxy.newProxyInstance(
                t.getClass().getClassLoader(),
                t.getClass().getInterfaces(),
                new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                before(method);
                Object o = method.invoke(t, args);
                after();
                return o;
            }
        });
    }

    private static void before(Method method) {
        System.out.println("前置操作");
        if("printAnimal".equals(method.getName())) {
            System.out.println("请问这是什么动物，它叫什么？");
        }
    }

    private static void after() {
        System.out.println("后置操作");
    }

    public static void main(String[] args) {
        AnimalService animalService = getProxy(new AnimalServiceImpl());
        System.out.println(animalService.printAnimal(new Animal().setType("猫").setName("汤姆")));
    }
}
