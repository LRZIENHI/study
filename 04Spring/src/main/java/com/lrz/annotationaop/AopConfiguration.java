package com.lrz.annotationaop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * <p>标题： </p>
 * <p>功能：
 *  @EnableAspectJAutoProxy 标签 用于替换xml配置中的 <aop:aspectj-autoproxy/>
 * </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月21日 </p>
 * <p>类全名：com.lrz.annotationaop.AopConfiguration</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Configuration
@ComponentScan(value = "com.lrz.annotationaop")
@EnableAspectJAutoProxy
public class AopConfiguration {
    public AopConfiguration() {
        System.out.println("容器启动初始化....");
    }
}
