package com.lrz.annotationaop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月21日 </p>
 * <p>类全名：com.lrz.annotationaop.AnnotationMyAspect</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Component
@Aspect
public class AnnotationMyAspect {
    @Before(value = "execution(String com.lrz.annotationaop.AnnotationAop.toString())")
    public void log() {
        System.out.println("记录日志============");
    }

    /**
     * 环绕通知
     * @param point
     * @return
     */
    @Around(value = "execution(String com.lrz.annotationaop.AnnotationAop.toString())")
    public Object aroundAdvice(ProceedingJoinPoint point) {
        Object[] args = point.getArgs();
        Object rtn = null;
        try {
            System.out.println("前置通知===========");
            rtn = point.proceed(args);
            System.out.println("后置通知===========");
        } catch (Throwable throwable) {
            System.out.println("异常通知===========");
        } finally {
            System.out.println("最终通知===========");
        }
        return rtn;
    }


    /**
     * 通用切入点测试
     */
    @Pointcut(value = "execution(String com.lrz.annotationaop.AnnotationAop.toString())")
    public void fn() {

    }

    @Before(value = "AnnotationMyAspect.fn()")
    public void monitor() {
        System.out.println("监控============");
    }

    @Before(value = "AnnotationMyAspect.fn()")
    public void asd() {
        System.out.println("权限验证============");
    }

}
