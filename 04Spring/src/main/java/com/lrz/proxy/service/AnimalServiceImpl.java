package com.lrz.proxy.service;

import com.lrz.proxy.model.Animal;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月20日 </p>
 * <p>类全名：com.lrz.proxy.service.AnimalServiceImpl</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class AnimalServiceImpl implements AnimalService {
    @Override
    public String printAnimal(Animal animal) {
        return animal.toString();
    }
}
