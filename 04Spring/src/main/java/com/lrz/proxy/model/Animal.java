package com.lrz.proxy.model;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月20日 </p>
 * <p>类全名：com.lrz.proxy.model.Animal</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
@Accessors(chain = true)
public class Animal {
    private String name;
    private String type;

    @Override
    public String toString() {
        return String.format("这是一只%s,它叫做%s", type, name);
    }
}
