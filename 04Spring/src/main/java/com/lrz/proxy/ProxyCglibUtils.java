package com.lrz.proxy;

import com.lrz.proxy.model.Animal;
import com.lrz.proxy.service.AnimalService;
import com.lrz.proxy.service.AnimalServiceImpl;
import org.springframework.cglib.proxy.Callback;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月20日 </p>
 * <p>类全名：com.lrz.proxy.ProxyCglibUtils</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class ProxyCglibUtils {
    public static AnimalService getProxy(AnimalService service) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(service.getClass());
        enhancer.setCallback(new MethodInterceptor() {

            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                if("printAnimal".equals(method.getName())) {
                    System.out.println("请问这是什么动物，它叫什么？");
                }
                return methodProxy.invokeSuper(o, objects);
//                return method.invoke(service, objects);
            }
        });
        AnimalService proxy = (AnimalService)enhancer.create();
        return proxy;
    }

    public static void main(String[] args) {
        AnimalService animalService = getProxy(new AnimalServiceImpl());
        System.out.println(animalService.printAnimal(new Animal().setType("猫").setName("汤姆")));
    }
}
