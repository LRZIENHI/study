package com.lrz.proxy;

import com.lrz.proxy.model.Animal;
import com.lrz.proxy.service.AnimalService;
import com.lrz.proxy.service.AnimalServiceImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * <p>标题：基于JDK的代理对象 </p>
 * <p>功能： 动物</p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月20日 </p>
 * <p>类全名：com.lrz.proxy.ProxyJDKUtils</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class ProxyJDKUtils {
    public static AnimalService getProxy(final AnimalService service) {
        AnimalService proxy = (AnimalService) Proxy.newProxyInstance(
                service.getClass().getClassLoader(),
                service.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if("printAnimal".equals(method.getName())) {
                            System.out.println("请问这是什么动物，它叫什么？");
                        }
                        //注意此处对象不能使用proxy，使用proxy会死循环
                        return method.invoke(service, args);
                    }
                }
        );
        return  proxy;
    }

    public static void main(String[] args) {
        AnimalService animalService = getProxy(new AnimalServiceImpl());
        System.out.println(animalService.printAnimal(new Animal().setType("猫").setName("汤姆")));
    }
}
