package com.lrz.mixedaop;

import org.springframework.stereotype.Component;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月20日 </p>
 * <p>类全名：com.lrz.mixedaop.MixedAop</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Component
public class MixedAop {
    @Override
    public String toString() {
        return "测试混合配置的AOP";
    }
}
