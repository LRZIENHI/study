package com.lrz.annotationConfiguration.Controller;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月18日 </p>
 * <p>类全名：com.lrz.annotationConfiguration.Controller.UserController</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
@Controller
//@PropertySource(value = "classpath:user.properties", encoding = "UTF-8")
public class UserController {
    @Value("${user.id}")
    private int id;
    @Value("${user.name1}")
    private String name;

    public String printUser() {
        return String.format("id=%s,name=%s", id, name);
    }
}
