package com.lrz.annotationConfiguration;

import com.lrz.annotationConfiguration.Controller.UserController;
import com.lrz.annotationConfiguration.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * <p>标题：  </p>
 * <p>功能：
 *  1、使用@Configuration定义的配置类，可以替换注解xml配置文件
 *  2、使用@Bean替换<bean/>标签
 *  3、使用@ComponentScan替换<context:component-scan/>标签
 *  4、使用@PropertySource替换<context:property-placeholder/>标签
 *  5、使用@Import替换<import/>标签，引入@Configuration定义的配置类
 * </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月18日 </p>
 * <p>类全名：com.lrz.annotationConfiguration.SpringConfiguration</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Configuration
@ComponentScan(basePackages = "com.lrz.annotationConfiguration.Controller")
@Import(value = {ImportConfiguration.class})
public class SpringConfiguration {
    public SpringConfiguration() {
        System.out.println("容器启动初始化....");
    }
    @Bean
    public User user() {
        return new User().setId(1).setName("赵四");
    }
}
