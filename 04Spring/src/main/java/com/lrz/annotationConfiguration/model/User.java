package com.lrz.annotationConfiguration.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月18日 </p>
 * <p>类全名：com.lrz.annotationConfiguration.Controller.User</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Component
@Data
@Accessors(chain = true)
public class User {
    private int id;
    private String name;

    @Override
    public String toString() {
        return String.format("id=%s,name=%s",id, name);
    }
}
