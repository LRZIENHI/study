package com.lrz.annotationConfiguration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月19日 </p>
 * <p>类全名：com.lrz.annotationConfiguration.ImportConfiguration</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Configuration
@PropertySource(value = "classpath:user.properties", encoding = "UTF-8")
public class ImportConfiguration {
}
