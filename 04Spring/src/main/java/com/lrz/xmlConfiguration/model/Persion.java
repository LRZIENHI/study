package com.lrz.xmlConfiguration.model;

import lombok.Data;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月16日 </p>
 * <p>类全名：com.lrz.model.Persion</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
public class Persion {
    private int id;
    private String name;
    private int sex;

    public Persion() {
        this.id = 1;
        this.name = "无参构造";
        this.sex = 1;
    }

    public Persion(int id,String name,int sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return String.format("id=%s,name=%s,sex=%s", id, name, 1==sex?"男":"女");
    }
}
