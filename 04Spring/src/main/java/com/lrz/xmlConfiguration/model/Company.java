package com.lrz.xmlConfiguration.model;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月17日 </p>
 * <p>类全名：com.lrz.model.Company</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
public class Company {
    public Company(){}
    public Company(String name,Persion persion) {
        this.name = name;
        this.persion = persion;
    }
    //简单类型注入
    private String name;
    //依赖类型注入ref
    private Persion persion;
    //集合类型注入
    private Map<String,String> persionid2name;
    private List<String> persionNames;
    private Set<Integer> persionIds;
    private Properties properties;
    @Override
    public String toString() {
        return String.format("name=%s,Persion=%s,map=%s,list=%s,set=%s,properties=%s",
                name, persion==null?"":persion.toString(),
                persionid2name==null?"":persionid2name.toString(),
                persionNames==null?"":persionNames.toString(),
                persionIds==null?"":persionIds.toString(),
                properties==null?"":properties.toString()
        );
    }
}
