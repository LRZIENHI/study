package com.lrz.xmlConfiguration.factory;

import com.lrz.xmlConfiguration.model.Persion;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月17日 </p>
 * <p>类全名：com.lrz.factory.PersionFactory</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class PersionFactory {

//    静态工厂方法
    public static Persion createPersion() {
        return new Persion(3, "静态工厂方法", 2);
    }
    //实例工厂方法
    public Persion createPersion2() {
        return new Persion(4, "实例工厂方法", 1);
    }
}
