package com.lrz.tx.service;

import com.lrz.tx.dao.AccountDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月21日 </p>
 * <p>类全名：com.lrz.tx.service.AccountServiceImpl</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Transactional
    @Override
    public void transfer(String from, String to, double money) {
        // 先查询from账户的钱
        double fromMoney = accountDao.queryMoney(from);
        // 对from账户进行扣钱操作
        accountDao.update(from, fromMoney - money);

        // 手动制造异常
        // System.out.println(1/0);
        // 先查询from账户的钱
        double toMoney = accountDao.queryMoney(to);
        // 对to账户进行加钱操作
        accountDao.update(to, toMoney + money);
    }

    @Resource
    private AccountDao accountDao;

}
