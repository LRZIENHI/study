package com.lrz.tx.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月21日 </p>
 * <p>类全名：com.lrz.tx.dao.AccountDaoImpl</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Repository
public class AccountDaoImpl extends JdbcDaoSupport implements AccountDao {
    // 完成数据源的自动装配
    @Autowired
    public AccountDaoImpl(DataSource dataSource) {
        setDataSource(dataSource);
    }

    @Override
    public void update(String name, double money) {
        Object[] args = { money, name };
        this.getJdbcTemplate().update("UPDATE account SET money = ? WHERE name = ? ", args);
    }

    @Override
    public double queryMoney(String name) {

        Double money = this.getJdbcTemplate().queryForObject("SELECT money FROM account WHERE name = ?",
                new DoubleMapper(), name);
        return money;
    }

}

// 结果映射器
class DoubleMapper implements RowMapper<Double> {

    @Override
    public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
        return rs.getDouble("money");
    }

}
