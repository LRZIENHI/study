package com.lrz.service;

import lombok.AllArgsConstructor;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月05日 </p>
 * <p>类全名：com.lrz.service.WrapService</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@AllArgsConstructor
public class WrapService {
    private String before;
    private String after;

    public String wrap(String word) {
        return before + word + after;
    }
}
