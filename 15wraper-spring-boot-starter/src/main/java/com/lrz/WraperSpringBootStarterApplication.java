package com.lrz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WraperSpringBootStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(WraperSpringBootStarterApplication.class, args);
	}

}
