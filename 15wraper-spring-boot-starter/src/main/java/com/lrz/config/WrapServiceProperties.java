package com.lrz.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>标题： </p>
 * <p>功能：读取配置文件中的 </p>
 * <p>版权： ACEWILL 2020</p>prefix和suffix
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月05日 </p>
 * <p>类全名：com.lrz.config.WrapServiceProperties</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
@ConfigurationProperties("wrap.service")
public class WrapServiceProperties {
    private String prefix;
    private String suffix;
}
