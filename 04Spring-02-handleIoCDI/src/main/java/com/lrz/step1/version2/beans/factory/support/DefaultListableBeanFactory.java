package com.lrz.step1.version2.beans.factory.support;

import com.google.common.collect.Maps;
import com.lrz.beans.factory.config.BeanDefinition;
import com.lrz.step1.version2.beans.factory.BeanFactory;
import com.lrz.step1.version2.beans.factory.config.BeanDefinitionRegistry;

import java.util.Map;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月01日 </p>
 * <p>类全名：com.lrz.step1.version2.beans.factory.support.DefaultListableBeanFactory</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class DefaultListableBeanFactory extends AbstractAutowireCapableBeanFactory implements BeanDefinitionRegistry {

    Map<String, BeanDefinition> map = Maps.newHashMap();

    @Override
    public void registryBeanDefinition(String beanName, BeanDefinition beanDefinition) {
        map.put(beanName, beanDefinition);
    }

    @Override
    public BeanDefinition getBeanDefinition(String beanName) {
        return map.get(beanName);
    }
}
