package com.lrz.step1.version2.beans.factory.xml;

import com.lrz.step1.version2.beans.factory.config.BeanDefinitionRegistry;
import com.lrz.step1.version2.beans.factory.support.DefaultListableBeanFactory;
import com.lrz.util.DocumentReader;
import org.dom4j.Document;

import java.io.InputStream; /**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月01日 </p>
 * <p>类全名：com.lrz.step1.version2.beans.factory.xml.XmlBeanDefinitionReader</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class XmlBeanDefinitionReader {
    private BeanDefinitionRegistry beanDefinitionRegistry;
    public XmlBeanDefinitionReader(BeanDefinitionRegistry beanDefinitionRegistry) {
        this.beanDefinitionRegistry = beanDefinitionRegistry;
    }

    public void loadBeanDefinition(InputStream inputStream) {
        Document document = DocumentReader.createDocument(inputStream);
        XmlBeanDefinitionDocumentReader documentReader = new XmlBeanDefinitionDocumentReader(beanDefinitionRegistry);
        documentReader.registerBeanDefinitions(document.getRootElement());
    }
}
