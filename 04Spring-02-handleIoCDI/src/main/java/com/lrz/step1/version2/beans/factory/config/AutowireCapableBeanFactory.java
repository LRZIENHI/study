package com.lrz.step1.version2.beans.factory.config;

import com.lrz.step1.version2.beans.factory.BeanFactory;

public interface AutowireCapableBeanFactory extends BeanFactory {
}
