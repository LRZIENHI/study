package com.lrz.step1.version2.beans.factory.config;

public interface SingletonBeanRegistry {
    void registerSingleton(String beanName, Object bean);
    Object getSingleton(String beanName);
}
