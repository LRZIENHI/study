package com.lrz.step1.version2;

import com.amazonaws.util.StringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lrz.beans.PropertyValue;
import com.lrz.beans.factory.config.BeanDefinition;
import com.lrz.beans.factory.config.RuntimeBeanReference;
import com.lrz.beans.factory.config.TypedStringValue;
import com.lrz.model.User;
import com.lrz.step1.version2.beans.factory.config.SingletonBeanRegistry;
import com.lrz.step1.version2.beans.factory.support.DefaultListableBeanFactory;
import com.lrz.step1.version2.beans.factory.support.DefaultSingletonBeanRegistry;
import com.lrz.step1.version2.beans.factory.xml.XmlBeanDefinitionReader;
import com.lrz.step1.version2.core.io.ClassPathResource;
import com.lrz.step1.version2.core.io.Resource;
import com.lrz.util.DocumentUtils;
import org.dom4j.Document;
import org.dom4j.Element;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * <p>标题： 版本2 </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月25日 </p>
 * <p>类全名：com.lrz.step1.Test1</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class Test2 {
    public static void main(String[] args) throws Exception {
        Test2 t = new Test2();
        User user = (User) t.getBean("user2");
//        user.setId(1).setName("五");
        System.out.println(user.toString());
    }

    public Object getBean(String beanName) throws Exception {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);
        Resource resource = new ClassPathResource("applicationContext.xml");
        InputStream inputStream = resource.getResource();
        xmlBeanDefinitionReader.loadBeanDefinition(inputStream);
        Object bean = beanFactory.getBean(beanName);
        return bean;
    }

}
