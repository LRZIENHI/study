package com.lrz.step1.version2.beans.factory;

public interface BeanFactory {
    Object getBean(String beanName);
}
