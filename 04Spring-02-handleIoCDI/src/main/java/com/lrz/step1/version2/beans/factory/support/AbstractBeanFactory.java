package com.lrz.step1.version2.beans.factory.support;

import com.lrz.beans.factory.config.BeanDefinition;
import com.lrz.step1.version2.beans.factory.BeanFactory;
import com.lrz.step1.version2.beans.factory.config.SingletonBeanRegistry;

import java.util.Map;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月01日 </p>
 * <p>类全名：com.lrz.step1.version2.beans.factory.support.AbstractBeanFactory</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public abstract class AbstractBeanFactory extends DefaultSingletonBeanRegistry implements BeanFactory{
    @Override
    public Object getBean(String beanName) {
        return doGetBean(beanName);
    }

    protected Object doGetBean(String beanName) {
        //1、查询单列bean集合中是否存在bean实例，有则取集合中的bean实例
        Object bean = getSingleton(beanName);
        if(bean!=null) {
            return bean;
        }
        //2、无则查询BeanDefinition集合中是否存在对应的BeanDefinition，有则通过BeanDefination创建bean实例
        BeanDefinition beanDefinition = getBeanDefinition(beanName);
        //3、无BeanDefinition则返回空
        if (beanDefinition == null) {
            return null;
        }
        bean = createBean(beanDefinition);
        if(beanDefinition.isSingleton()) {
            registerSingleton(beanName, bean);
        }
        return bean;
    }

    protected abstract Object createBean(BeanDefinition beanDefinition);

    protected abstract BeanDefinition getBeanDefinition(String beanName);


}
