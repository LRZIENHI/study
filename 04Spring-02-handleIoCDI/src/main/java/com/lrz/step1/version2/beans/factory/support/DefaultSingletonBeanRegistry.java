package com.lrz.step1.version2.beans.factory.support;

import com.google.common.collect.Maps;
import com.lrz.step1.version2.beans.factory.config.SingletonBeanRegistry;

import java.util.Map;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月01日 </p>
 * <p>类全名：com.lrz.step1.version2.beans.factory.support.DefaultSingletonBeanRegistry</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class DefaultSingletonBeanRegistry implements SingletonBeanRegistry {
    //单列对象集合
    Map<String,Object> singletonBeanMap = Maps.newHashMap();
    @Override
    public void registerSingleton(String beanName, Object bean) {
        singletonBeanMap.put(beanName, bean);
    }

    @Override
    public Object getSingleton(String beanName) {
        return singletonBeanMap.get(beanName);
    }
}
