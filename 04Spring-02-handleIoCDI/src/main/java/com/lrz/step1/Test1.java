package com.lrz.step1;

import com.amazonaws.util.StringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lrz.beans.PropertyValue;
import com.lrz.beans.factory.config.BeanDefinition;
import com.lrz.beans.factory.config.RuntimeBeanReference;
import com.lrz.beans.factory.config.TypedStringValue;
import com.lrz.model.User;
import com.lrz.util.DocumentUtils;
import org.dom4j.Document;
import org.dom4j.Element;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月25日 </p>
 * <p>类全名：com.lrz.step1.Test1</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class Test1 {
    public static void main(String[] args) throws Exception {
        Test1 t = new Test1();
        User user = (User) t.getBean("user2");
//        user.setId(1).setName("五");
        System.out.println(user.toString());
    }
    //单列对象集合
    Map<String,Object> singletonBeanMap = Maps.newHashMap();
    public Object getBean(String beanName) throws Exception {
        Object bean = singletonBeanMap.get(beanName);
        if(bean!=null) {
            return bean;
        }
        Map<String,BeanDefinition> map = loadConfiguration();
        BeanDefinition beanDefinition = map.get(beanName);
        if (beanDefinition == null) {
            return null;
        }
        //实例化new
        String classType = beanDefinition.getClassType();
        Class<?> clazz = Class.forName(classType);
        Constructor<?> constructor = clazz.getConstructor();
        Object obj = constructor.newInstance();
        if(beanDefinition.isSingleton()) {
            singletonBeanMap.put(beanName, obj);
        }
        //set属性填充
        List<PropertyValue> propertyValueList = beanDefinition.getPropertyValueList();
        if(propertyValueList!=null && propertyValueList.size()>0) {
            for (PropertyValue propertyValue : propertyValueList) {
                String name = propertyValue.getName();
                Object value = propertyValue.getValue();
                Field declaredField = clazz.getDeclaredField(name);
                declaredField.setAccessible(true);
                Object vlaueToUse = null;
                if(value instanceof TypedStringValue) {
                    vlaueToUse = ((TypedStringValue)value).getValue();
                } else if (value instanceof RuntimeBeanReference) {
                    vlaueToUse = getBean(((RuntimeBeanReference)value).getRefName());
                }
                declaredField.set(obj, vlaueToUse);
            }
        }
        //初始化init
        String initMethod = beanDefinition.getInitmethed();
        if (!StringUtils.isNullOrEmpty(initMethod)) {
           Method method = clazz.getMethod(initMethod);
           method.invoke(obj);
        }
        return obj;
    }

    public Map<String, BeanDefinition> loadConfiguration() {

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("applicationContext.xml");
        Document document = DocumentUtils.readDocument(inputStream);
        Element element = document.getRootElement();
        Map<String, BeanDefinition> map = parseConfiguration(element);
        return map;
    }

    private Map<String, BeanDefinition> parseConfiguration(Element element) {
        List<Element> beanList = element.elements();
        Map<String, BeanDefinition> map = Maps.newHashMap();
        for (Element bean : beanList) {
            String beanName = bean.attributeValue("id");
            String classtype = bean.attributeValue("class");
            String scope = bean.attributeValue("scope");
            String initmethed = bean.attributeValue("init-methed");
            BeanDefinition beanDefinition = new BeanDefinition(beanName, classtype)
                    .setScope(scope).setInitmethed(initmethed);
            map.put(beanName, beanDefinition);
            List<Element> properties = bean.elements();
            if(properties!=null && properties.size()>0) {
                List<PropertyValue> propertyValues = Lists.newArrayList();
                beanDefinition.setPropertyValueList(propertyValues);
                for (Element property : properties) {
                    String name = property.attributeValue("name");
                    String ref = property.attributeValue("ref");
                    if(!StringUtils.isNullOrEmpty(ref)) {
                        PropertyValue propertyValue = new PropertyValue(name, new RuntimeBeanReference().setRefName(ref));
                        propertyValues.add(propertyValue);
                        continue;
                    }
                    String value = property.attributeValue("value");
                    PropertyValue propertyValue = new PropertyValue(name, new TypedStringValue().setValue(value));
                    propertyValues.add(propertyValue);
                }
            }
        }
        return map;
    }
}
