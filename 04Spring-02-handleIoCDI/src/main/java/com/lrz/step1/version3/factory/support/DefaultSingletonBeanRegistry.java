package com.lrz.step1.version3.factory.support;

import com.google.common.collect.Maps;
import com.lrz.step1.version3.factory.config.SingletonBeanRegistry;

import java.util.Map;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月05日 </p>
 * <p>类全名：com.lrz.step1.version3.factory.support.DefaultSingletonBeanRegistry</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class DefaultSingletonBeanRegistry implements SingletonBeanRegistry {
    Map<String, Object> singletonBeanMap = Maps.newHashMap();
    @Override
    public void registrySingletonBean(String number, Object bean) {
        singletonBeanMap.put(number, bean);
    }

    @Override
    public Object getSingletonBean(String number) {
        return singletonBeanMap.get(number);
    }
}
