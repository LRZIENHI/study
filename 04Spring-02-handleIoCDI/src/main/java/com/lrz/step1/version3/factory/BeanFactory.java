package com.lrz.step1.version3.factory;

public interface BeanFactory {
    Object getBean(String beanName);
}
