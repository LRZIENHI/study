package com.lrz.step1.version3;


import com.lrz.model.User;
import com.lrz.step1.version3.factory.core.io.ClassPathResource;
import com.lrz.step1.version3.factory.support.DefaultListableBeanFactory;
import com.lrz.step1.version3.factory.xml.XmlBeanDefinitionReader;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月25日 </p>
 * <p>类全名：com.lrz.step1.Test1</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class Test3 {
    public static void main(String[] args) throws Exception {
        Test3 t = new Test3();
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);
        ClassPathResource classPathResource = new ClassPathResource("applicationContext.xml");
        xmlBeanDefinitionReader.loadBeanDefinition(classPathResource.getResource());
        User user = (User) beanFactory.getBean("user2");
//        user.setId(1).setName("五");
        System.out.println(user.toString());
    }



}
