package com.lrz.step1.version3.factory.xml;

import com.lrz.step1.version3.factory.BeanFactory;
import com.lrz.step1.version3.factory.config.BeanDefinitionRegistry;
import com.lrz.step1.version3.factory.support.DefaultListableBeanFactory;
import com.lrz.util.DocumentReader;
import org.dom4j.Document;

import java.io.InputStream;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月04日 </p>
 * <p>类全名：com.lrz.step1.version3.factory.xml.XmlBeanDefinitionReader</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class XmlBeanDefinitionReader {
    private BeanDefinitionRegistry beanFactory;
    public XmlBeanDefinitionReader(BeanDefinitionRegistry beanFactory) {
        this.beanFactory = beanFactory;
    }

    public void loadBeanDefinition(InputStream inputStream) {
        Document document = DocumentReader.createDocument(inputStream);
        XmlBeanDefinitionDocumentReader documentReader = new XmlBeanDefinitionDocumentReader(beanFactory);
        documentReader.loadBeanDefinition(document.getRootElement());
    }
}
