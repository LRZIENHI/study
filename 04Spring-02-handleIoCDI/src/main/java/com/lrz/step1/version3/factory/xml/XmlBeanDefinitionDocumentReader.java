package com.lrz.step1.version3.factory.xml;

import com.amazonaws.util.StringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lrz.beans.PropertyValue;
import com.lrz.beans.factory.config.BeanDefinition;
import com.lrz.beans.factory.config.RuntimeBeanReference;
import com.lrz.beans.factory.config.TypedStringValue;
import com.lrz.step1.version3.factory.config.BeanDefinitionRegistry;
import com.lrz.step1.version3.factory.support.DefaultListableBeanFactory;
import org.dom4j.Element;

import java.util.List;
import java.util.Map;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月04日 </p>
 * <p>类全名：com.lrz.step1.version3.factory.xml.XmlBeanDefinitionDocumentReader</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class XmlBeanDefinitionDocumentReader {
    private BeanDefinitionRegistry beanFactory;
    public XmlBeanDefinitionDocumentReader(BeanDefinitionRegistry beanFactory) {
        this.beanFactory = beanFactory;
    }

    public void loadBeanDefinition(Element element) {
        List<Element> beanList = element.elements();
        for (Element bean : beanList) {
            String beanName = bean.attributeValue("id");
            String classtype = bean.attributeValue("class");
            String scope = bean.attributeValue("scope");
            String initmethed = bean.attributeValue("init-methed");
            BeanDefinition beanDefinition = new BeanDefinition(beanName, classtype)
                    .setScope(scope).setInitmethed(initmethed);
            beanFactory.registryBeanDefinition(beanName, beanDefinition);
            List<Element> properties = bean.elements();
            if(properties!=null && properties.size()>0) {
                List<PropertyValue> propertyValues = Lists.newArrayList();
                beanDefinition.setPropertyValueList(propertyValues);
                for (Element property : properties) {
                    String name = property.attributeValue("name");
                    String ref = property.attributeValue("ref");
                    if(!StringUtils.isNullOrEmpty(ref)) {
                        PropertyValue propertyValue = new PropertyValue(name, new RuntimeBeanReference().setRefName(ref));
                        propertyValues.add(propertyValue);
                        continue;
                    }
                    String value = property.attributeValue("value");
                    PropertyValue propertyValue = new PropertyValue(name, new TypedStringValue().setValue(value));
                    propertyValues.add(propertyValue);
                }
            }
        }
    }
}
