package com.lrz.step1.version3.factory.support;

import com.lrz.beans.factory.config.BeanDefinition;
import com.lrz.step1.version3.factory.BeanFactory;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月03日 </p>
 * <p>类全名：com.lrz.step1.version3.factory.support.AbstractBeanFactory</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public abstract class AbstractBeanFactory extends DefaultSingletonBeanRegistry implements BeanFactory {
    @Override
    public Object getBean(String beanName) {
        return doGetBean(beanName);
    }

    private Object doGetBean(String beanName) {
        //1、从缓存中获取bean
        Object bean = getSingletonBean(beanName);
        if(bean!=null) {
            return bean;
        }
        //2、如果缓存中没有，从beanDefinition缓存中缓存beanDefinition后创建bean
        BeanDefinition beanDefinition = getBeanDefinition(beanName);
        if(beanDefinition==null) {
            return null;
        }
        bean = createBean(beanDefinition);
        //3、如果bean不为空，且为单列bean，放入bean缓存
        if(beanDefinition.isSingleton()) {
            registrySingletonBean(beanName, bean);
        }
        return bean;
    }

    protected abstract BeanDefinition getBeanDefinition(String beanName);

    protected abstract Object createBean(BeanDefinition beanDefinition);



}
