package com.lrz.step1.version3.factory.support;

import com.amazonaws.util.StringUtils;
import com.lrz.beans.PropertyValue;
import com.lrz.beans.factory.config.BeanDefinition;
import com.lrz.beans.factory.config.RuntimeBeanReference;
import com.lrz.beans.factory.config.TypedStringValue;
import com.lrz.util.ReflectUtils;

import java.util.List;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年07月04日 </p>
 * <p>类全名：com.lrz.step1.version3.factory.support.AbstractAutowireCapableBeanFactory</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory{
    @Override
    protected Object createBean(BeanDefinition beanDefinition) {
        //实例化new
        String classType = beanDefinition.getClassType();
        Object object = ReflectUtils.createObject(classType);
        //set属性填充
        List<PropertyValue> propertyValueList = beanDefinition.getPropertyValueList();
        if(propertyValueList!=null && propertyValueList.size()>0) {
            for (PropertyValue propertyValue : propertyValueList) {
                String name = propertyValue.getName();
                Object value = propertyValue.getValue();
                Object vlaueToUse = null;
                if(value instanceof TypedStringValue) {
                    vlaueToUse = ((TypedStringValue)value).getValue();
                } else if (value instanceof RuntimeBeanReference) {
                    vlaueToUse = this.getSingletonBean(((RuntimeBeanReference)value).getRefName());
                }
                ReflectUtils.setProperty(object, name, vlaueToUse);
            }
        }
        //初始化init
        String initMethod = beanDefinition.getInitmethed();
        if (!StringUtils.isNullOrEmpty(initMethod)) {
            ReflectUtils.invokeMethod(object, initMethod);
        }
        return object;
    }
}
