package com.lrz.step1.version3.factory.config;

public interface SingletonBeanRegistry {
    void registrySingletonBean(String number, Object bean);
    Object getSingletonBean(String number);
}
