package com.lrz.beans.factory.config;

import com.lrz.beans.PropertyValue;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月25日 </p>
 * <p>类全名：com.lrz.beans.factory.config.BeanDefinition</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
@Accessors(chain = true)
public class BeanDefinition {
    private String id;
    private String classType;
    private List<PropertyValue> propertyValueList;
    private String scope;
    private String initmethed;

    public BeanDefinition(String id, String classType) {
        this.id = id;
        this.classType = classType;
    }

    public boolean isSingleton() {
        return scope==null || "singleton".equals(scope);
    }
}
