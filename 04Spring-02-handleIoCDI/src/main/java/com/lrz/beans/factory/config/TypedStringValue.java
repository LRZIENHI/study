package com.lrz.beans.factory.config;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年06月25日 </p>
 * <p>类全名：com.lrz.beans.factory.config.TypedStringValue</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
@Accessors(chain = true)
public class TypedStringValue {
    private Class<?> targetType;
    private String value;

    public Object getValue() {
        if(this.value == null) {
            return null;
        }
        if(targetType == null || targetType == String.class) {
            return this.value;
        } else if(targetType == Integer.class) {
            return Integer.valueOf(this.value);
        }
        return null;
    }
}
