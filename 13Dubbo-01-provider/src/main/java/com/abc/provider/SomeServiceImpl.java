package com.abc.provider;

import com.abc.service.SomeService;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年04月26日 </p>
 * <p>类全名：com.abc.provider.SomeServiceImpl</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class SomeServiceImpl implements SomeService {
    @Override
    public String hello(String name) {
        System.out.println("我是提供者111");
        return "hello dubbo world!"+name;
    }
}
