package cn.acewill.lrz.logger;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class LoggerTest {

    private static Logger logger = LoggerFactory.getLogger(LoggerTest.class);

    public static void main(String arg[]) {
//        BasicConfigurator.configure();
        LoggerTest test = new LoggerTest();
        test.testLogger();
    }

    public void testLogger() {
        logger.debug("debug：....");
        logger.info("info...");
        logger.error("error...");
    }
}
