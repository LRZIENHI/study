package acewill.lrz.map;

import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2019</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2019年08月08日 </p>
 * <p>类全名：acewill.lrz.map.HashMapTest</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class HashMapTest {
    public static void main(String[] args) {
        HashMap<String,String> map = Maps.newHashMap();
        HashMapTest test = new HashMapTest();
        test.findMapTableSize(map);
    }

    public void findMapTableSize(HashMap<String,String> map) {
        for(int i=0;i<20;i++) {
            map.put(i+1+"",i*10+"");
        }
        System.out.println(map.get("2"));
    }
}
