package acewill.lrz.map;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.TreeMap;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2019</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2019年09月10日 </p>
 * <p>类全名：acewill.lrz.map.TreeMapTest</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class TreeMapTest {
    public static void main(String[] args) {
        TreeMap<String,String> treeMap = Maps.newTreeMap();
        for(int i=0;i<20;i++) {
            int r = (int) (Math.random()*100);
            System.out.println(r);
            treeMap.put(r+"",r*10+"");
        }
    }
}
