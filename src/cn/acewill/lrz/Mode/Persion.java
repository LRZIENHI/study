package acewill.lrz.Mode;

import lombok.Data;

/**
 * <p>标题： </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2019</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2019年04月01日 </p>
 * <p>类全名：acewill.lrz.Mode.Persion</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
@Data
public class Persion {
    private String id;
    private String name;
    private int age;
    public String toString(){
        return id + ":" + name + ":" + age;
    }
}
