package cn.acewill.lrz.thread;

public class RunnableTest implements Runnable{

    public static void main(String[] args) {
        Runnable runnable = new RunnableTest();
        Thread oneThread = new Thread(runnable);
        oneThread.start();
    }

    @Override
    public void run() {
        System.out.println("线程运行！");
    }
}
