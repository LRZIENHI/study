package cn.acewill.lrz.thread;

public class ThreadTest {
    public static void main(String[] org) throws Exception {
        //Thread实现Runnable,再执行start
        Long time1 = System.currentTimeMillis();
        Thread t2 = new Thread(new Runnable(){
            @Override
            public void run() {
                System.out.println("线程运行！");
            }
        });
        t2.start();
        Long time2 = System.currentTimeMillis();
        //用lambda表达式执行
        Thread t1 = new Thread(()->System.out.println("线程运行！"));
        t1.start();
        Long time3 = System.currentTimeMillis();
        System.out.println("1==="+(time2-time1));
        System.out.println("2==="+(time3-time2));
    }

}
