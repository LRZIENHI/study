package cn.acewill.lrz.eleme.getTokenDemo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("ElemeServlet")
public class ElemeTokenCallbackURLServlet extends HttpServlet{
    /**
     * 饿了么授权回调在此处响应，主要是获取授权码code
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().write("<br>ElemeServlet.doGet..");
        String method=request.getParameter("method");
        //响应授权回调
        if("auth_back".equals(method)){
            String code=request.getParameter("code");
            String state=request.getParameter("state");
            response.getWriter().write("<br>code:"+code);
            response.getWriter().write("<br>state:"+state);
        }
        response.getWriter().flush();
        response.getWriter().close();
    }
}
