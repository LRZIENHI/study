package cn.acewill.lrz.graphics;

/**
 * 图片数据
 */
public enum PicData {
    DATA1(630,320,10,10,150,300,320,10,300,300,10,206,300,14);

    private int imageWidth;//宽
    private int imageHeight;//高
    private int x_LogoPic;//LogoX坐标
    private int y_LogoPic;//LogoY坐标
    private int H_LogoPic;//Logo宽度
    private int W_LogoPic;//Logo高度
    private int x_QRcode;//二维码X坐标
    private int y_QRcode;//二维码Y坐标
    private int H_QRcode;//二维码宽度
    private int W_QRcode;//二维码高度
    private int x_text;//文本起始Y坐标
    private int y_text;//文本起始Y坐标
    private int W_text;//文本宽度
    private int textSize;//文本字体大小

    PicData(int imageWidth, int imageHeight, int x_LogoPic, int y_LogoPic, int H_LogoPic, int W_LogoPic
            , int x_QRcode, int y_QRcode, int H_QRcode, int W_QRcode,int x_text, int y_text, int W_text, int textSize
    ) {
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.x_LogoPic = x_LogoPic;
        this.y_LogoPic = y_LogoPic;
        this.H_LogoPic = H_LogoPic;
        this.W_LogoPic = W_LogoPic;
        this.x_QRcode = x_QRcode;
        this.y_QRcode = y_QRcode;
        this.H_QRcode = H_QRcode;
        this.W_QRcode = W_QRcode;
        this.x_text = x_text;
        this.y_text = y_text;
        this.W_text = W_text;
        this.textSize = textSize;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public int getX_LogoPic() {
        return x_LogoPic;
    }

    public int getY_LogoPic() {
        return y_LogoPic;
    }

    public int getH_LogoPic() {
        return H_LogoPic;
    }

    public int getW_LogoPic() {
        return W_LogoPic;
    }

    public int getX_QRcode() {
        return x_QRcode;
    }

    public int getY_QRcode() {
        return y_QRcode;
    }

    public int getH_QRcode() {
        return H_QRcode;
    }

    public int getW_QRcode() {
        return W_QRcode;
    }

    public int getX_text() {
        return x_text;
    }

    public int getY_text() {
        return y_text;
    }

    public int getW_text() {
        return W_text;
    }

    public int getTextSize() {
        return textSize;
    }
}
