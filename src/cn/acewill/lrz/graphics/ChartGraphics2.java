package cn.acewill.lrz.graphics;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class ChartGraphics2 {
    //生成图片文件
    @SuppressWarnings("restriction")
    public void createImage(String fileLocation, BufferedImage image) {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        if(image != null){
            try {
                fos = new FileOutputStream(fileLocation);
                bos = new BufferedOutputStream(fos);
                JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(bos);
                encoder.encode(image);
                bos.close();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }finally{
                if(bos!=null){//关闭输出流
                    try {
                        bos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(fos!=null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void graphicsGeneration(String imgurl) {
        PicData data = PicData.DATA1;
        int H_QRcode = data.getH_QRcode();  //二维码高度
        int W_QRcode = data.getW_QRcode();  //二维码宽度
        int H_LogePic = data.getH_LogoPic();  //Loge高度
        int W_LogePic = data.getW_LogoPic();  //Loge宽度

        int imageWidth =  data.getImageWidth();
        int imageHeight = data.getImageHeight();
        BufferedImage image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        //设置图片的背景色
        Graphics2D main = image.createGraphics();
        main.setColor(Color.white);
        main.fillRect(0, 0, imageWidth, imageHeight);
        //***********************Loge图片
        Graphics mainPic = image.createGraphics();
        BufferedImage bimg = null;
        try {
            bimg = javax.imageio.ImageIO.read(new java.io.File(imgurl));
        } catch (Exception e) {}
        if(bimg!=null){
            //Loge图片
            mainPic.drawImage(bimg, data.getX_LogoPic(), data.getY_LogoPic(), W_LogePic, H_LogePic, null);
            //二维码图片
            mainPic.drawImage(bimg, data.getX_QRcode(), data.getY_QRcode(), W_QRcode, H_QRcode, null);
            mainPic.dispose();
        }
        //***********************设置下面的提示框
        Graphics2D tip = image.createGraphics();
        //设置字体颜色，先设置颜色，再填充内容
        tip.setColor(Color.black);
        //设置字体
        int textSize = data.getTextSize();
        int x_text = data.getX_text();
        int y_text = data.getY_text();
        int W_text = data.getW_text();
        Font tipFont = new Font("宋体", Font.PLAIN, textSize);
        tip.setFont(tipFont);
        String text1 = "资产编码";
        tip.drawString(text1, x_text + (W_text-getStringWidth(tip, text1, tipFont))/2, y_text);
        String text2 = "-----XXXXXXXXXXX-----";
        tip.drawString(text2, x_text + (W_text-getStringWidth(tip, text2, tipFont))/2, y_text + textSize);
        createImage("d:\\javaCreate\\hehe.jpg", image);

    }

    private int getStringWidth(Graphics g, String text, Font font) {
        FontMetrics metrics = g.getFontMetrics(font);
        return metrics.stringWidth(text);
    }

    public static void main(String[] args) {
        ChartGraphics2 cg = new ChartGraphics2();
        try {
            cg.graphicsGeneration("D:\\javaCreate\\11.jpg");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
