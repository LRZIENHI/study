package cn.acewill.lrz.lambda;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * lambda表达式语法：
 * () -> {}
 * 1、() : 括号就是接口方法的括号，接口方法如果有参数，也需要写参数。只有一个参数时，括号可以省略。
 * 2、-> : 分割左右部分的，没啥好讲的。
 * 3、{} : 要实现的方法体。只有一行代码时，可以不加括号，可以不写return。
 * Java8中内置了四大核心函数型接口
 * 1、消费型接口（有参无返回值）
 * Consumer<T>
 *     void accept(T t);
 * 2、供给型接口（无参有返回值）
 * Supplier<T>
 *     T get();
 * 3、函数型接口（有参有返回值）
 * Function<T, R>
 *     R apply(T t);
 * 4、断言型接口（有参有布尔返回值）
 * Predicate<T>
 *     boolean test(T t);
 */
public class LambdaTest {
    public static void main(String[] org) throws Exception {
        Function function1 = (x) -> x+"10";
        System.out.println(function1.apply("1"));
        System.out.println(function1.apply("null"));
        /**
         * 方法引用的语法：
         *     对象::实例方法
         *     类::静态方法
         *     类::实例方法
         */
        Function function2 = String::valueOf;
        System.out.println(function2.apply(1000));
        System.out.println(function2.apply(null));
        Supplier<String> supplier = () -> new String();
        System.out.println(supplier.get().getClass().getName());
    }
}
