package com.outlook.lrzienhi.charp01;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;


/**
 * <p>标题：定义服务器启动类 </p>
 * <p>功能：构建服务器启动对象ServerBootStrap </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年04月02日 </p>
 * <p>类全名：com.outlook.lrzienhi.charp01.NettyDemo01</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
public class NettyHttpServer {
    public static void main(String[] args) {
        System.nanoTime();
        /**
         * 用于处理客户端连接请求，将请求发送给childGroup中的eventLoop
         */
        EventLoopGroup parentGroup = new NioEventLoopGroup();
        /**
         * 用于处理客户端请求
         */
        EventLoopGroup childGroup = new NioEventLoopGroup();
        //用户启动ServerChanne
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(parentGroup, childGroup)//指定eventLoop
                .channel(NioServerSocketChannel.class)//指定使用NIO通讯
                .childHandler(new HttpChannelInitializer())//指定childGroup中的eventLoop所绑定的线程所要处理的处理器
        ;
        try {
            //指定当前服务器所监听的端口号
            //bind()方法执行是异步的
            ChannelFuture future = serverBootstrap.bind(8888)
                    .sync();//sync()方法会使bind()操作与后续的代码的执行由异步变为了同步
            System.out.println("服务器启动成功。监听的端口号为：8888");
            //关闭Channel
            //closeFuture()方法执行是异步的，当future调用了close()方法并关闭成功后，才会触发closeFuture()的执行
            future.channel().closeFuture()
                    .sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //优雅关闭
            parentGroup.shutdownGracefully();
            childGroup.shutdownGracefully();
        }

    }
}
