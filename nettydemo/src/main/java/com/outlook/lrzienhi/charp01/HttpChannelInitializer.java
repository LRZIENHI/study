package com.outlook.lrzienhi.charp01;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

/**
 * <p>标题： 管道初始化器 </p>
 * <p>功能： </p>
 * <p>版权： ACEWILL 2020</p>
 * <p>公司: 奥琦玮信息科技(北京)有限公司</p>
 * <p>创建日期：2020年04月08日 </p>
 * <p>类全名：com.outlook.lrzienhi.charp01.HttpChannelInitializer</p>
 * <p>
 * 作者：李仁志
 * 初审：
 * 复审：
 *
 * @version 1.0
 */
//当前类的实例再piepeline初始化之后被GC，一般使用匿名内部类
public class HttpChannelInitializer extends ChannelInitializer<SocketChannel> {

    //当Channel初始化创建完毕后就会触发该方法的执行，用于初始化Channel
    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        //从Channel中获取到pipeline
        ChannelPipeline pipeline = channel.pipeline();
        //将HttpServerCodec处理器放入到pipeline的最后
        //HttpServerCodec是解码器HttpRequestDecoder和编码器HttpResponseEncoder的结合体
        //HttpRequestDecoder:http请求解码器，将Channel中的ByteBuffer数据解码为HttpRequest对象
        //HttpResponseEncoder:http响应编码器，将HttpResponse对象编码为将要在Channel中发送的ByteBuffer数据
        pipeline.addLast("HttpServerCodec", new HttpServerCodec());
        //再将自定义的处理器放入到pipeline的最后
        pipeline.addLast("HttpServerHandler", new HttpServerHandler());
    }
}
